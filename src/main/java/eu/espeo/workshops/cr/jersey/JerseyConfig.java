package eu.espeo.workshops.cr.jersey;

import org.glassfish.jersey.server.ResourceConfig;

import javax.annotation.PostConstruct;
import javax.inject.Named;
import javax.ws.rs.ApplicationPath;

@Named
@ApplicationPath("/")
public class JerseyConfig extends ResourceConfig {

    @PostConstruct
    public void registerPackage() {
        packages("eu.espeo.workshops.cr");
    }
}
