package eu.espeo.workshops.cr.jersey;

import lombok.Value;

@Value
class Error {
    String message;

    static Error errorMessage(String message) {
        return new Error(message);
    }
}
