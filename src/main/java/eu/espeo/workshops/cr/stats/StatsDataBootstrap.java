package eu.espeo.workshops.cr.stats;

import static java.util.UUID.randomUUID;

import eu.espeo.workshops.cr.util.RandomNumberGenerator;
import lombok.RequiredArgsConstructor;

import java.util.stream.IntStream;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.inject.Named;

@Named
@RequiredArgsConstructor(onConstructor = @__(@Inject))
public class StatsDataBootstrap {

    private final StatsRepository statsRepository;
    private final RandomNumberGenerator randomGenerator;

    @PostConstruct
    public void init() {
        IntStream.range(0, 10)
                .forEach(i -> statsRepository.add(randomStats()));
    }

    private Stats randomStats() {
        return Stats.builder()
                .reviewId(randomUUID())
                .loc(randomGenerator.randomInt(1000))
                .comments(randomGenerator.randomInt(30))
                .resolved(randomGenerator.randomInt(30))
                .build();
    }
}
