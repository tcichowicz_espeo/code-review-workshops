package eu.espeo.workshops.cr.repository;

public interface Entity<Id> {

    Id getId();
}
