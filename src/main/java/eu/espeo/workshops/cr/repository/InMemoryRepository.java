package eu.espeo.workshops.cr.repository;

import static com.google.common.base.Preconditions.checkArgument;
import static java.util.stream.Collectors.toList;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

public abstract class InMemoryRepository<T extends Entity<K>, K> implements Repository<T, K> {

    private final Map<K, T> store = new HashMap<>();

    @Override
    public void add(T entity) {
        checkArgument(!store.containsKey(entity.getId()), "Entity already exists for id " + entity.getId());
        store.put(entity.getId(), entity);
    }

    @Override
    public void update(T entity) {
        checkArgument(store.containsKey(entity.getId()), "No entity found for id " + entity.getId());
        store.put(entity.getId(), entity);
    }

    @Override
    public void remove(K id) {
        store.remove(id);
    }

    @Override
    public Collection<T> query(Specification<T> specification) {
        return store.values().stream()
                .filter(specification::matches)
                .collect(toList());
    }

    @Override
    public Optional<T> findById(K id) {
        return Optional.ofNullable(store.get(id));
    }
}
